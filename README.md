![Image](https://www.uscis.gov/sites/default/files/images/social-media/OPA-SM_USCISonBlue_FB_1200x627_V1.png)

# Naturalization Interview Confidence Environment ( N I C E )
---

[Naturalization Interview Confidence Environment](https://gitlab.com/worcester/cs/naturalization-interview-confidence-environment) - A Literacy App for the Program Director for [Literacy Volunteers of the Montachusett Area](https://lvmonta.org/) to assist one of their classes in order to prepare for the US citezenship interview. This app is intended to assist people with low digital literacy who may be unfamiliar with having to read off a tablet, and even more by having to write on a tablet with a stylus or finger.
## This `Documentation` contains details about the Naturalization Interview Confidence Environment's design, development processes, and licensing.
---
### Licenses — We license all our code under [GPLv3](http://a.com) and all other content under [CC-BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)
---
Copyright © 2021 The LibreFoodPantry Authors. This work is licensed  under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit [https://creativecommons.org/licenses/by-sa/4.0/](https://creativecommons.org/licenses/by-sa/4.0/).